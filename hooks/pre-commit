#!/bin/bash

yarn up knockout-schema@https://gitlab.com/knockout-community/knockout-schema.git
git add yarn.lock

# ############################## #
# ESLint check:
# ############################## #

STAGED_FILES=$(git diff --cached --name-only --diff-filter=ACM | grep ".[jt]sx\{0,1\}$")
ESLINT="$(git rev-parse --show-toplevel)/node_modules/.bin/eslint"

if [[ "$STAGED_FILES" = "" ]]; then
  echo "No files"
  exit 0
fi

PASS=true

printf "\nValidating Javascript:\n"

# Check for eslint
if [[ ! -x "$ESLINT" ]]; then
  printf "\t\033[41mPlease install ESlint\033[0m (yarn add eslint -D)"
  exit 1
fi

for FILE in $STAGED_FILES
do
  "$ESLINT" "$FILE" --fix

  if [[ "$?" == 0 ]]; then
    printf "\t\033[32mESLint Passed: $FILE\033[0m\n"
  else
    printf "\t\033[41mESLint Failed: $FILE\033[0m\n"
    PASS=false
  fi
done

printf "\nJavascript validation completed!\n"

if ! $PASS; then
  printf "\033[41mCOMMIT FAILED:\033[0m Your commit contains files that should pass ESLint but do not. Please fix the ESLint errors and try again.\n"
  exit 1
else
  printf "\033[42mESLINT SUCCEEDED\033[0m\n"
fi

# ############################## #
# Prettier check:
# ############################## #

PRETTIER="$(git rev-parse --show-toplevel)/node_modules/.bin/prettier"
# Check for eslint
if [[ ! -x "$PRETTIER" ]]; then
  printf "\t\033[41mPlease install Prettier\033[0m (yarn add prettier -D)"
  exit 1
fi

FILES=$(git diff --cached --name-only --diff-filter=ACM "*.js" "*.jsx" | sed 's| |\\ |g')
[ -z "$FILES" ] && exit 0

# Prettier check all selected files
echo "$FILES" | xargs ./node_modules/.bin/prettier --check

# Add back the modified/prettified files to staging
echo "$FILES" | xargs git add

# ############################## #
# stylelint check:
# ############################## #

STYLELINT="$(git rev-parse --show-toplevel)/node_modules/.bin/stylelint"
# Check for eslint
if [[ ! -x "$STYLELINT" ]]; then
  printf "\t\033[41mPlease install stylelint\033[0m (yarn add stylelint -D)"
  exit 1
fi

printf "\nValidating stylelint:\n"
for FILE in $STAGED_FILES
do
  "$STYLELINT" "$FILE"

  if [[ "$?" == 0 ]]; then
    printf "\t\033[32mstylelint passed: $FILE\033[0m"
  else
    printf "\t\033[41mstylelint failed: $FILE\033[0m"
    PASS=false
  fi
done

if ! $PASS; then
  printf "\033[41mSTYLELINT FAILED:\033[0m Your commit contains files that should pass stylelint but do not. Please fix the stylelint errors and try again.\n"
  exit 1
else
  printf "\033[42mSTYLELINT SUCCEEDED\033[0m\n"
fi

# Add back the modified/prettified files to staging
echo "$FILES" | xargs git add

exit $?
