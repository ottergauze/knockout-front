import React from 'react';
import styled, { keyframes } from 'styled-components';
import { ThemeFontSizeLarge } from '../../utils/ThemeNew';
import StyleableLogo from '../Header/components/StyleableLogo';

const messages = [
  'Reticulating Splines...',
  'Node Graph out of date. Rebuilding...',
  'Sending client info...',
  'hl2.exe is not responding...',
  'i see you.',
];

const Loading = () => (
  <StyledLoadingContainer>
    <div className="loading-content">
      <div className="logo-container">
        <StyleableLogo className="loading-logo" />
      </div>
      <div className="loading-text">{messages[Math.floor(Math.random() * messages.length)]}</div>
    </div>
  </StyledLoadingContainer>
);

const loadingAnim = keyframes`
  0% {
    transform: scale(1);
    opacity: 1;
  }

  50% {
    transform: scale(0.85);
    opacity: 0.1;
  }

  100% {
    transform: scale(1);
    opacity: 1;
  }
`;

const StyledLoadingContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: calc(100vh - 110px);

  .loading-content {
    margin-bottom: 15vh;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }

  .logo-container {
    margin-bottom: 20px;
  }

  .loading-logo {
    width: auto;
    height: 200px;
    shape-rendering: geometricprecision;
    margin-left: 35px;
    animation: ${loadingAnim} 2.3s ease-in-out infinite;
  }

  .loading-text {
    font-weight: bold;
    text-align: center;
    font-size: ${ThemeFontSizeLarge};
  }
`;
export default Loading;
