import React, { useEffect, useState } from 'react';
import Modal from '../Modals/Modal';
import { FieldLabelSmall, TextField } from '../FormControls';
import { editBanReason } from '../../services/moderation';

interface EditBanReasonModalProps {
  banId: number;
  submitFn: (banReason: string) => void;
  cancelFn: () => void;
  isOpen: boolean;
}

const EditBanReasonModal: React.FC<EditBanReasonModalProps> = ({
  banId,
  submitFn,
  cancelFn,
  isOpen,
}) => {
  const [banReason, setBanReason] = useState('');
  const [disableSubmit, setDisableSubmit] = useState(false);

  const updateBanReason = async () => {
    await editBanReason({ banId, banReason });
  };

  useEffect(() => {
    setDisableSubmit(banReason.length < 1);
  }, [banReason]);

  return (
    <Modal
      iconUrl="/static/icons/siren.png"
      title="Edit Ban Reason"
      cancelFn={cancelFn}
      submitFn={async () => {
        setDisableSubmit(true);
        await updateBanReason();
        submitFn(banReason);
        setDisableSubmit(false);
      }}
      isOpen={isOpen}
      disableSubmit={disableSubmit}
    >
      <FieldLabelSmall>Ban Reason</FieldLabelSmall>
      <TextField
        aria-label="new-ban-reason"
        value={banReason}
        maxLength={250}
        onChange={(e) => setBanReason(e.target.value)}
      />
    </Modal>
  );
};

export default EditBanReasonModal;
