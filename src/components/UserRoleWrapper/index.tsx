import React from 'react';
import styled from 'styled-components';

import { User } from 'knockout-schema';
import userRoles, { goldMemberFallbackColor } from './userRoles';
import { isDeletedUser } from '../../utils/deletedUser';

interface StyledUserRoleWrapperProps {
  userRole: { name: string; color: string; extraStyle: string };
  isBanned: boolean;
  username: string;
}

export const StyledUserRoleWrapper = styled.span<StyledUserRoleWrapperProps>`
  color: ${(props) => props.userRole.color};

  ${(props) => props.userRole.extraStyle};

  ${(props) => isDeletedUser(props.username) && 'color: #999999;'}

  overflow: hidden;
  text-overflow: ellipsis;

  ${(props) =>
    props.userRole.color === goldMemberFallbackColor &&
    `.role-icon {
    color: ${props.userRole.color};
  }`}
`;

interface UserRoleWrapperProps {
  user: User;
  className?: string;
  children: React.ReactNode;
}
const UserRoleWrapper = ({ user, className = '', children }: UserRoleWrapperProps) => {
  const userRoleCode = user?.role?.code || 'basic-user';
  const userRole = userRoles[userRoleCode] ? userRoles[userRoleCode] : userRoles['basic-user'];
  const isBanned = !!user?.banned;
  const title = `${user.username} - ${userRole.name}${user?.online ? ' (Online)' : ''}`;

  return (
    <StyledUserRoleWrapper
      className={`user-role-wrapper-component ${className}`}
      userRole={userRole}
      title={title}
      isBanned={isBanned}
      username={user.username}
    >
      {children}
    </StyledUserRoleWrapper>
  );
};

export default UserRoleWrapper;
