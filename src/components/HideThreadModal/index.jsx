import React from 'react';
import PropTypes from 'prop-types';
import Modal from '../Modals/Modal';

const HideThreadModal = ({ modalOpen, setModalOpen, threadTitle, hideThread }) => {
  return (
    <Modal
      title={`Hide "${threadTitle}"?`}
      submitText="Hide"
      submitFn={hideThread}
      cancelFn={() => setModalOpen(false)}
      isOpen={modalOpen}
    >
      <span>
        You can manage hidden threads in your
        <a href="/usersettings" target="_blank">
          {` User Settings.`}
        </a>
      </span>
    </Modal>
  );
};

HideThreadModal.propTypes = {
  modalOpen: PropTypes.bool.isRequired,
  setModalOpen: PropTypes.func.isRequired,
  threadTitle: PropTypes.string.isRequired,
  hideThread: PropTypes.func.isRequired,
};

export default HideThreadModal;
