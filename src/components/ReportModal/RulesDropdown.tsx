import React from 'react';
import styled from 'styled-components';
import { Rule } from 'knockout-schema';
import { ThemeBackgroundLighter, ThemeVerticalPadding } from '../../utils/ThemeNew';
import ModalSelect from '../Modals/ModalSelect';

const getRuleLabel = (rule) =>
  `${rule.rulableType || 'Site-wide'} Rule ${rule.cardinality + 1}: ${rule.title}`;

const RuleSelect = styled.div`
  .rule-label {
    margin-top: ${ThemeVerticalPadding};
    background: ${ThemeBackgroundLighter};
  }
`;

interface RulesDropdownProps {
  rules: Rule[];
  onSelect: (e) => void;
}

const RulesDropdown = ({ rules, onSelect }: RulesDropdownProps) => {
  let options = rules.map((rule) => {
    const label = getRuleLabel(rule);
    return {
      text: label,
      value: label,
    };
  });

  // we don't use `defaultText` in the `modalSelect` as
  // we need the default option to have a blank value
  options = [
    {
      text: 'Select a Rule',
      value: '',
    },
  ].concat(options);

  return (
    <RuleSelect>
      <ModalSelect className="rule-label" options={options} onChange={onSelect} />
    </RuleSelect>
  );
};

export default RulesDropdown;
