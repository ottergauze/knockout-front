/* eslint-disable no-use-before-define */
import React from 'react';
import { ShortcodeTree, ShortcodeNode, TextNode } from 'shortcode-tree';

import styled from 'styled-components';
import { transparentize } from 'polished';
import Schema from './Schema';
import { ThemePostLineHeight, ThemeVerticalPadding } from '../../utils/ThemeNew';
import UserRoleWrapper from '../UserRoleWrapper';
import userRoles from '../UserRoleWrapper/userRoles';
import { DELETED_USER_USERNAME } from '../../utils/deletedUser';

const UNPARSED_TAGS = ['code', 'noparse'];
const NON_BLOCK_TAGS = ['b', 'i', 'u', 's', 'url', 'spoiler', 'noparse', 'img', 'video'];

const StyledParser = styled.div`
  padding: ${ThemeVerticalPadding} 0;
  line-height: ${ThemePostLineHeight}%;
`;

const StyledMention = styled.a`
  background: ${(props) => transparentize(0.9, props.roleColor(props))};
  padding: 1.5px 2px;
  margin-right: 2px;
  font-weight: bold;

  &:hover {
    background: ${(props) =>
      transparentize(props.theme.mode === 'light' ? 0 : 0.7, props.roleColor(props))};
    ${(props) =>
      props.theme.mode === 'light' ? ` .user-role-wrapper-component { color: white; }` : ''}
  }
`;

// Returns if the node should ignore newlines
// directly after the node's closing tag.
function shouldCheckNewLine(node) {
  return node instanceof ShortcodeNode && !NON_BLOCK_TAGS.includes(node.shortcode.name);
}

const MENTION_REGEX = /@<(\d+;?.*?)>/g;
const MENTION_GROUP_REGEX = /(@<\d+;?.*?>)/g;

const replaceMentionsWithLinks = (content, mentionUsers = []) => {
  if (!content) return content;

  const splitContent = content.split(MENTION_GROUP_REGEX);

  if (splitContent.length < 2) {
    return content;
  }

  // we cannot use Map.groupBy because some browsers do not support it
  const mentionUsersMap = new Map();
  mentionUsers.forEach((mentionUser) => {
    const { id } = mentionUser;
    if (!mentionUsersMap.has(id)) {
      mentionUsersMap.set(id, []);
    }
    mentionUsersMap.get(id).push(mentionUser);
  });

  return splitContent.reduce((arr, element) => {
    if (!element) return arr;

    // if element doesnt start with @, return the element
    if (element[0] !== '@') {
      return [...arr, element];
    }

    const components = element.split(MENTION_REGEX);

    // if element doesnt contain a mention, return the element
    if (components.length < 2) {
      return [...arr, element];
    }

    const userId = Number(components[1].split(';')[0]);
    const mentionUser = mentionUsersMap.get(userId)?.[0];
    const isDeletedUser = mentionUser?.username === DELETED_USER_USERNAME;

    // if user doesnt exist or is deleted, return the element
    if (!mentionUser || isDeletedUser) {
      return [...arr, element];
    }

    const {
      username,
      role: { code: roleCode },
    } = mentionUser;

    const role = userRoles[roleCode] ?? userRoles['basic-user'];
    const roleColor = typeof role.color === 'function' ? role.color : () => role.color;
    return [
      ...arr,
      <StyledMention key={userId} href={`/user/${userId}`} roleColor={roleColor}>
        <UserRoleWrapper key={userId} user={{ id: userId, username, role: { code: roleCode } }}>
          {`@${username}`}
        </UserRoleWrapper>
      </StyledMention>,
    ];
  }, []);
};

function parseTree(node, index, mentions = [], checkNewLine = false, spoiler = false) {
  if (node instanceof TextNode || (node instanceof ShortcodeNode && node.shortcode === null)) {
    const trimNewLine = checkNewLine && node.text && node.text[0] === '\n';
    const textWithMentionLinks = trimNewLine
      ? replaceMentionsWithLinks(node.text.substring(1), mentions)
      : replaceMentionsWithLinks(node.text, mentions);
    return <span key={`${node.text}-${index}`}>{textWithMentionLinks}</span>;
  }

  if (node instanceof ShortcodeNode) {
    const { properties } = node.shortcode;
    const tag = node.shortcode.name;

    // return just the text contents of tags in UNPARSED_TAGS. prevents breakage
    if (UNPARSED_TAGS.includes(tag)) {
      return (
        <Schema
          key={`${tag}-${index}`}
          tag={tag}
          content={node.text}
          properties={{ ...properties, key: `${tag}-${index}` }}
          raw={node.shortcode.codeText}
        />
      );
    }

    // dont mention in spoiler tags
    let content = tag === 'spoiler' ? node.text : replaceMentionsWithLinks(node.text, mentions);

    if (node.children.length === 0 && node.text) {
      return (
        <Schema
          key={`${tag}-${index}`}
          tag={tag}
          content={content}
          properties={{ ...properties, key: `${tag}-${index}` }}
          raw={node.shortcode.codeText}
          spoiler={spoiler}
        />
      );
    }

    content = (
      <>
        {node.children.map((child, i) =>
          parseTree(
            child,
            i,
            mentions,
            i > 0 && shouldCheckNewLine(node.children[i - 1]),
            tag === 'spoiler'
          )
        )}
      </>
    );

    return (
      <Schema
        key={`${tag}-${index}`}
        tag={tag}
        content={content}
        properties={{ ...properties, key: `${tag}-${index}` }}
        raw={node.shortcode.codeText}
        hasChildren
      />
    );
  }

  return null;
}

export const bbToTree = (string) => ShortcodeTree.parse(string);

const Parser = ({ content, mentionUsers = [] }) => {
  try {
    const shortCodes = ShortcodeTree.parse(content);

    const comps =
      shortCodes.children.length > 0
        ? shortCodes.children.map((node, index) =>
            parseTree(
              node,
              index,
              mentionUsers,
              index > 0 && shouldCheckNewLine(shortCodes.children[index - 1])
            )
          )
        : parseTree(shortCodes, 1, mentionUsers);

    return <StyledParser>{comps}</StyledParser>;
  } catch (error) {
    console.error(error);
    return <pre>Could not parse BBcode.</pre>;
  }
};
export default Parser;
