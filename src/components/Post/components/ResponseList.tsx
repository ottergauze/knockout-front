import { Post, User } from 'knockout-schema';
import React, { useState } from 'react';
import styled from 'styled-components';
import { useHistory } from 'react-router-dom';
import MiniUserInfo from '../../MiniUserInfo';
import { ThemeFontSizeMedium, ThemeHorizontalPadding } from '../../../utils/ThemeNew';
import { TextButton } from '../../Buttons';
import UserAvatar from '../../Avatar';
import stripQuotes from '../../../utils/stripQuotes';
import { bbToTree } from '../../KnockoutBB/Parser';
import replaceTags, { TagReplacements } from '../../../utils/replaceTags';

const StyledResponseList = styled.div`
  padding: 0 calc(${ThemeHorizontalPadding} * 1.5);
  font-size: ${ThemeFontSizeMedium};
  min-width: 0;

  .tag-placeholder {
    opacity: 0.6;
    vertical-align: middle;
    margin-right: 5px;
    font-size: 20px;
  }

  .response-list-text {
    opacity: 0.6;
  }

  .avatar-preview {
    width: 28px;
    margin-right: 5px;
  }

  .reply-text {
    margin: 0 3px;
  }

  .arrow {
    padding-top: 1px;
  }

  .response-list-menu {
    display: flex;
    align-items: center;
    padding: 0;
    margin-bottom: 10px;
  }

  .response {
    display: flex;
    align-items: center;
    cursor: pointer;
    user-select: none;
    margin-bottom: 7px;
    max-width: 95%;

    .response-content {
      margin-left: 5px;
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
      line-height: normal;
    }
  }
`;

interface ResponseListProps {
  responses: Post[];
  threadId: number;
  postId: number;
  page: number;
}

const truncateContentToReply = (tree, postId) => {
  let { children } = tree;
  for (let i = 0; i < tree.children.length; i++) {
    const child = tree.children[i];

    if (
      child.shortcode?.name === 'quote' &&
      Number(child.shortcode?.properties.postId) === postId
    ) {
      children = tree.children.slice(i + 1);
      break;
    }
  }

  return {
    ...tree,
    children,
  };
};

const ResponseList = ({ responses, threadId, page, postId }: ResponseListProps) => {
  const [open, setOpen] = useState(false);
  const history = useHistory();

  const handlePostResponseClick = async (post: Post) => {
    // scroll into view if post is on the same page
    if (post.thread?.id === threadId && post.page === page) {
      const element = document.getElementById(`post-${post.id}`);
      element?.scrollIntoView({ behavior: 'smooth', block: 'center' });
    } else {
      history.push(`/thread/${post.thread?.id}/${post.page}#post-${post.id}`);
    }
  };

  const hasAvatar = (user: User) =>
    (user?.avatarUrl?.length ?? 0) !== 0 && !user.avatarUrl.includes('none.webp');

  const avatars = responses
    .filter((response) => hasAvatar(response.user!))
    .slice(0, 3)
    .map((response) => response.user!.avatarUrl);

  const tagsToReplace: TagReplacements = {
    img: <i className="fa-solid fa-image tag-placeholder" />,
    youtube: <i className="fa-solid fa-film tag-placeholder" />,
    video: <i className="fa-solid fa-film tag-placeholder" />,
    spoiler: <i className="fa-solid fa-eye-slash tag-placeholder" />,
  };

  return (
    <StyledResponseList>
      <TextButton className="response-list-menu" onClick={() => setOpen((value) => !value)}>
        {avatars.map((avatar) => (
          <UserAvatar key={avatar} className="avatar-preview" src={avatar} />
        ))}
        <span className="response-list-text reply-text">
          {`${responses.length} ${responses.length === 1 ? 'reply' : 'replies'}`}
        </span>
        <i className={`fa-solid fa-chevron-${open ? 'down' : 'right'} response-list-text arrow`} />
      </TextButton>

      {open && (
        <div className="response-list-items">
          {responses.map((response) => (
            <div
              key={response.id}
              className="response"
              tabIndex={0}
              role="button"
              onKeyUp={(e) => {
                if (e.key === 'Enter') handlePostResponseClick(response);
              }}
              onClick={() => handlePostResponseClick(response)}
            >
              <MiniUserInfo user={response.user!} defaultAvatar />
              <span className="response-content">
                {replaceTags(
                  stripQuotes(truncateContentToReply(bbToTree(response.content), postId)),
                  tagsToReplace
                )}
              </span>
            </div>
          ))}
        </div>
      )}
    </StyledResponseList>
  );
};

export default ResponseList;
