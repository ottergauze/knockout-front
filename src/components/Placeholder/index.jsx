import React from 'react';
import PropTypes from 'prop-types';
import styled, { keyframes } from 'styled-components';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeFontSizeHeadline,
  ThemeFontSizeHuge,
  ThemeFontSizeLarge,
  ThemeFontSizeMedium,
  ThemeFontSizeSmall,
} from '../../utils/ThemeNew';

const Placeholder = ({ width, height, marginBottom, marginTop, textSize }) => (
  <StyledPlaceholder
    width={width}
    height={height}
    marginBottom={marginBottom}
    marginTop={marginTop}
    textSize={textSize}
  >
    <div className="placeholder" />
  </StyledPlaceholder>
);

Placeholder.propTypes = {
  width: PropTypes.number.isRequired,
  height: PropTypes.number,
  marginBottom: PropTypes.number,
  marginTop: PropTypes.number,
  textSize: PropTypes.string,
};

Placeholder.defaultProps = {
  marginBottom: 8,
  marginTop: undefined,
  height: undefined,
  textSize: '',
};

const placeholderAnim = keyframes`
  0% {
    transform: translateX(0%);
  }
  100% {
    transform: translateX(-37.5%);
  }
`;

export const StyledPlaceholder = styled.div`
  width: ${(props) => props.width}px;
  overflow: hidden;
  margin-bottom: ${(props) => props.marginBottom}px;
  margin-top: ${(props) => (props.marginTop ? `${props.marginTop}px` : 'unset')};

  .placeholder {
    display: block;
    width: 2000px;
    height: ${(props) => {
      if (props.height) return `${props.height}px`;
      if (props.textSize === 'small') return `calc(${ThemeFontSizeSmall(props)} * 0.95)`;
      if (props.textSize === 'large') return `calc(${ThemeFontSizeLarge(props)} * 0.95)`;
      if (props.textSize === 'huge') return `calc(${ThemeFontSizeHuge(props)} * 0.95)`;
      if (props.textSize === 'headline') return ThemeFontSizeHeadline(props);
      return `calc(${ThemeFontSizeMedium(props)} * 0.95)`;
    }};
    background: linear-gradient(
      90deg,
      ${ThemeBackgroundLighter} 0%,
      ${ThemeBackgroundDarker} 12.5%,
      ${ThemeBackgroundLighter} 37.5%,
      ${ThemeBackgroundDarker} 49.5%,
      ${ThemeBackgroundLighter} 74.5%,
      ${ThemeBackgroundDarker} 100%
    );
    animation: ${placeholderAnim} 1s linear infinite;
  }
`;

export default Placeholder;
