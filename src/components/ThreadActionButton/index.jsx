import React from 'react';
import PropTypes from 'prop-types';

import { TextButton } from '../Buttons';

const ThreadActionButton = ({ action, children }) => (
  <TextButton small onClick={action}>
    {children}
  </TextButton>
);

ThreadActionButton.propTypes = {
  action: PropTypes.func,
  children: PropTypes.string.isRequired,
};
ThreadActionButton.defaultProps = {
  action: undefined,
};

export default ThreadActionButton;
