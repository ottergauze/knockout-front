export function modifySelectionRange(input, cursorPosition) {
  setTimeout(() => {
    input.setSelectionRange(cursorPosition, cursorPosition);
    input.focus();
  }, 100);
}

export function selectionRangeValid(selectionRange) {
  return selectionRange[0] !== undefined && selectionRange[1] !== undefined;
}

export function selectionRangeIsCursor(selectionRange) {
  return selectionRange[0] === selectionRange[1];
}

export const getCaretCoordinates = (element, caretIndex) => {
  const text = element.value.substring(0, caretIndex);
  const mirrorDiv = document.createElement('div');
  mirrorDiv.style.position = 'absolute';
  mirrorDiv.style.top = '0';
  mirrorDiv.style.left = '0';
  mirrorDiv.style.width = 'auto';
  mirrorDiv.style.whiteSpace = 'pre-wrap';
  mirrorDiv.textContent = text;
  mirrorDiv.style.lineHeight = window.getComputedStyle(element).lineHeight;
  mirrorDiv.style.padding = window.getComputedStyle(element).padding;
  mirrorDiv.style.fontSize = window.getComputedStyle(element).fontSize;
  element.parentNode.appendChild(mirrorDiv);

  const caretSpan = document.createElement('span');
  caretSpan.textContent = '|';
  mirrorDiv.appendChild(caretSpan);

  const elementRect = element.getBoundingClientRect();
  const rect = caretSpan.getBoundingClientRect();
  const parentElement = element.parentNode;
  const parentElementRect = parentElement.getBoundingClientRect();

  const leftOffset = elementRect.left + window.scrollX;
  const adjustedLeft = rect.left - leftOffset;
  const parentScrollTop = parentElement.scrollTop;
  const adjustedTop = rect.top - parentElementRect.top + parentScrollTop;

  mirrorDiv.parentNode.removeChild(mirrorDiv);

  return { top: adjustedTop, left: adjustedLeft };
};
