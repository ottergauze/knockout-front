import { TextNode } from 'shortcode-tree';

const stripQuotes = (tree) => {
  if (tree.shortcode === null && tree.children && tree.children.length > 0) {
    return tree.children.map(stripQuotes).join('');
  }

  if (tree instanceof TextNode || tree.shortcode === null) {
    return tree.text;
  }

  if (tree.shortcode.name !== 'quote') {
    return tree.shortcode.codeText;
  }

  return '';
};

export default stripQuotes;
