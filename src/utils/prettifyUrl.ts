/**
 * Makes a URL prettier by removing the protocol (http/https) and 'www.' if present.
 *
 * @param url - The URL to be cleaned.
 *
 * @returns A cleaned and prettified version of the URL, or null if the input is falsy.
 *
 * @example
 * prettifyUrl('https://www.google.com');
 * // => 'google.com'
 */
export default (url?: string): string | null => {
  if (!url) return null;
  let prettyUrl = url.replace(/^(https?:)?\/\//, '');
  prettyUrl = prettyUrl.replace(/^www\./, '');
  return prettyUrl;
};
