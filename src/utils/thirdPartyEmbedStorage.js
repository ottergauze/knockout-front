export const loadEmbedSettingFromStorage = () => {
  const embedSetting = JSON.parse(localStorage.getItem(`embedSetting`));

  if (embedSetting) {
    return embedSetting;
  }
  return false;
};

export const saveEmbedSettingToStorage = () => {
  const embedSetting = JSON.stringify(true);

  localStorage.setItem(`embedSetting`, embedSetting);
  // eslint-disable-next-line no-restricted-globals
  location.reload();
};
