import dayjs from 'dayjs';

export default (banExpiration) => {
  const startTime = dayjs(new Date());
  const endTime = dayjs(banExpiration);

  const hoursLength = endTime.diff(startTime) / 3600000;
  // bans expiring more than 10 years from now are permabans
  return hoursLength > 87660;
};
