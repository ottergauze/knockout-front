import axios from 'axios';

import { GetSubforumThreadsResponse, Subforum } from 'knockout-schema';
import config from '../../config';

import { authGet } from './common';

export const getSubforumList = async (hideNsfw = false): Promise<Subforum[]> => {
  const res = await axios.get(`${config.apiHost}/v2/subforums${hideNsfw ? '?hideNsfw=1' : ''}`);

  const { data } = res;

  return data;
};

export const getSubforumWithThreads = async (
  subforumid: number,
  page = 1,
  hideNsfw = true
): Promise<GetSubforumThreadsResponse> => {
  const user = localStorage.getItem('currentUser');

  // if logged in, get the subforum with auth so
  // subscribed threads are fetched
  if (user) {
    const res = await authGet({
      url: `/v2/subforums/${subforumid}/${page}${hideNsfw ? '?hideNsfw=1' : ''}`,
    });

    const { data } = res;

    return data;
  }

  const res = await axios.get(
    `${config.apiHost}/v2/subforums/${subforumid}/${page}${hideNsfw ? '?hideNsfw=1' : ''}`
  );

  const { data } = res;

  return data;
};
