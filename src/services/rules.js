import axios from 'axios';

import config from '../../config';

export const getSiteRules = async () => {
  const res = await axios.get(`${config.apiHost}/v2/rules`);
  const { data } = res;

  return data;
};

export const getThreadRules = async (threadId) => {
  const res = await axios.get(`${config.apiHost}/v2/rules`, {
    params: { rulableType: 'Thread', rulableId: threadId },
  });
  const { data } = res;

  return data;
};

export const getSubforumRules = async (subforumId) => {
  const res = await axios.get(`${config.apiHost}/v2/rules`, {
    params: { rulableType: 'Subforum', rulableId: subforumId },
  });
  const { data } = res;

  return data;
};
