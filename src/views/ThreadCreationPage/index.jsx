/* eslint-disable react/forbid-prop-types */
import React, { useState, useEffect, useRef } from 'react';
import { useParams, useHistory, Redirect } from 'react-router-dom';

import { Helmet } from 'react-helmet';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import { getTags } from '../../services/tags';
import { submitThread } from './helpers';
import {
  checkPresence,
  checkBetweenLengths,
  checkIsImage,
  ifPresent,
  validate,
  checkRole,
} from '../../utils/forms';
import { pushSmartNotification } from '../../utils/notification';
import { POST_CHARACTER_LIMIT } from '../../utils/limits';

import IconSelector from '../../components/IconSelector';
import Tooltip from '../../components/Tooltip';
import UserRoleRestricted from '../../components/UserRoleRestricted';
import EditorBB from '../../components/EditorBB';
import InputError from '../../components/InputError';
import ThreadCreationNotice from './components/ThreadCreationNotice';

import StyledThreadCreation from './style';
import { Tag } from '../../components/Buttons';
import { isLoggedIn } from '../../components/LoggedInOnly';
import { getSubforumWithThreads } from '../../services/subforums';
import { threadSearch } from '../../services/threadSearch';
import { GOLD_USER_ROLES } from '../../utils/roleCodes';

dayjs.extend(relativeTime);

const threadValidators = {
  title: [checkPresence, (value) => checkBetweenLengths(value, 4, 140)],
  content: [checkPresence, (value) => checkBetweenLengths(value, 4, POST_CHARACTER_LIMIT)],
  selectedIconId: [checkPresence],
  backgroundUrl: [
    (value) => ifPresent(value, () => checkRole(GOLD_USER_ROLES)),
    (value) => ifPresent(value, checkPresence),
    (value) => ifPresent(value, checkIsImage),
  ],
};

const ThreadCreationComponent = () => {
  const [errors, setErrors] = useState({});
  const [selectedIconId, setSelectedIconId] = useState(undefined);
  const [title, setTitle] = useState('');
  const [backgroundUrl, setBackgroundUrl] = useState('');
  const [backgroundType, setBackgroundType] = useState('cover');
  const [tags, setTags] = useState([]);
  const [selectedTags, setSelectedTags] = useState([]);
  const [content, setContent] = useState('');
  const [submitting, setSubmitting] = useState(false);
  const [subforumValid, setSubforumValid] = useState(true);
  const [subforumName, setSubforumName] = useState('');
  const [threadSearchResults, setThreadSearchResults] = useState([]);
  const { id: subforumId } = useParams();
  const searchTimer = useRef();
  const history = useHistory();

  useEffect(() => {
    const grabTags = async () => {
      setTags(await getTags());
    };

    const checkSubforum = async () => {
      try {
        const { subforum } = await getSubforumWithThreads(subforumId);
        setSubforumName(subforum.name);
      } catch {
        setSubforumValid(false);
      }
    };

    grabTags();
    checkSubforum();
  }, []);

  useEffect(() => {
    const search = async () => {
      const { threads } = await threadSearch({
        title,
        sortBy: 'created_at',
      });
      setThreadSearchResults(threads.slice(0, 6));
    };

    clearTimeout(searchTimer.current);
    if (title) {
      searchTimer.current = setTimeout(search, 600);
    } else {
      setThreadSearchResults([]);
    }
  }, [title]);

  if (!isLoggedIn()) {
    window.location.replace('/login');
    return null;
  }

  const handleBackgroundTypeChange = (e) => {
    setBackgroundType(e.target.value);
  };

  const handleTagAdd = (e) => {
    const { value } = e.target;

    setSelectedTags([...selectedTags, value]);
    e.target.value = 'default';
  };

  const handleTagRemove = (index) => {
    setSelectedTags(selectedTags.filter((tag, i) => i !== index));
  };

  const getTagName = (str) => str.split('|')[1];

  const handleSubmit = async () => {
    setSubmitting(true);

    const formErrors = validate(
      {
        title,
        content,
        selectedIconId,
        backgroundUrl,
      },
      threadValidators
    );

    if (Object.keys(formErrors).length > 0) {
      setErrors(formErrors);
      pushSmartNotification('Unable to make thread, check the form for errors.');
    } else {
      await submitThread(
        title,
        content,
        selectedIconId,
        backgroundUrl,
        backgroundType,
        selectedTags,
        Number(subforumId),
        history
      );
    }
    setSubmitting(false);
  };

  if (!subforumValid) {
    return <Redirect to="/" />;
  }

  return (
    <StyledThreadCreation>
      <Helmet>
        <title>{`${title || 'Create New Thread'} - Knockout!`}</title>
      </Helmet>

      <h1>Create New Thread</h1>
      <p>{`Subforum: ${subforumName}`}</p>

      <ThreadCreationNotice
        subforumRulesLink={`/subforumRules/${subforumId}`}
        subforumName={subforumName}
      />

      <div className="input">
        <label className="input-label" aria-label="title">
          <span>Title</span>
          <input
            type="text"
            name="title"
            placeholder="Internet Drama"
            onChange={(e) => setTitle(e.target.value)}
            value={title}
          />
        </label>
      </div>
      <InputError error={errors.title} />
      {threadSearchResults.length > 0 && (
        <div className="search-results">
          <div className="results-header">Similar threads</div>
          {threadSearchResults.map((thread) => (
            <div className="thread-result" key={thread.id}>
              <a href={`/thread/${thread.id}`} target="_blank">
                <span className="thread-result-title">{thread.title}</span>
              </a>
              &nbsp;
              <span className="thread-result-subforum">{`in ${thread.subforum?.name}`}</span>
              <span className="thread-result-date">{dayjs(thread.createdAt).fromNow()}</span>
            </div>
          ))}
        </div>
      )}
      <div className="editor">
        <EditorBB
          content={content}
          setContent={(value) => setContent(value)}
          handleSubmit={handleSubmit}
        >
          <button
            type="submit"
            disabled={submitting}
            onClick={handleSubmit}
            title="Submit post (shortcut: ctrl+enter)"
          >
            <i className="fa-solid fa-paper-plane" />
            Submit
          </button>
        </EditorBB>
        <InputError error={errors.content} />
      </div>

      <IconSelector
        selectedId={selectedIconId}
        handleIconChange={(iconId) => setSelectedIconId(iconId)}
        error={errors.selectedIconId}
      />

      <UserRoleRestricted roleCodes={GOLD_USER_ROLES}>
        <div className="line-option-wrapper">
          <div className="input tall">
            <Tooltip fullWidth text="You will get absolutely BTFO if you abuse this!">
              <div className="tooltip-content">
                <label className="input-label" aria-label="background-url">
                  <span>Background URL (optional)</span>
                  <input
                    type="text"
                    name="background-url"
                    placeholder="https://onlinewebimages.com/i/epic_image.png"
                    onChange={(e) => setBackgroundUrl(e.target.value)}
                    value={backgroundUrl}
                  />
                </label>
              </div>
            </Tooltip>
          </div>

          <div className="input">
            <label className="input-label" htmlFor="background-type">
              <span>Background type</span>
              <div className="background-type-input">
                <div className="background-type-input-inner">
                  <Tooltip text="Cover (fills the whole page in the background)">
                    <label className={backgroundType === 'cover' ? 'active' : ''} htmlFor="cover">
                      <i className="fa-solid fa-expand-arrows-alt" />
                      <input
                        type="radio"
                        value="cover"
                        id="cover"
                        name="background-type"
                        aria-label="Background type - Cover"
                        checked={backgroundType === 'cover'}
                        onChange={handleBackgroundTypeChange}
                        hidden
                      />
                    </label>
                  </Tooltip>
                  <Tooltip text="Tiled (best for small images, gets repeated in the background)">
                    <label className={backgroundType === 'tiled' ? 'active' : ''} htmlFor="tiled">
                      <i className="fa-solid fa-puzzle-piece" />
                      <input
                        type="radio"
                        value="tiled"
                        id="tiled"
                        name="background-type"
                        aria-label="Background type - Tiled"
                        checked={backgroundType === 'tiled'}
                        onChange={handleBackgroundTypeChange}
                        hidden
                      />
                    </label>
                  </Tooltip>
                </div>
              </div>
            </label>
          </div>
        </div>
        <InputError error={errors.backgroundUrl} />
      </UserRoleRestricted>

      {tags.length > 0 && (
        <div className="line-option-wrapper">
          <div className="input tall tags-list">
            <label className="input-label" htmlFor="tags">
              <span>Tags</span>

              <select id="tags" onChange={handleTagAdd}>
                <option value="default">Select a tag...</option>
                {tags.map((tag) => (
                  <option
                    key={`tag_${tag.id}`}
                    value={`${tag.id}|${tag.name}`}
                    disabled={selectedTags.includes(`${tag.id}|${tag.name}`)}
                  >
                    {tag.name}
                  </option>
                ))}
              </select>
            </label>
          </div>
          <div className="input">
            <div className="input-label">
              <span>Selected Tags</span>
              <div className="selected-tags">
                {selectedTags.map((tag, index) => {
                  return (
                    <Tag key={tag} type="button" onClick={() => handleTagRemove(index)}>
                      <i className="fa-solid fa-times-circle" />
                      &nbsp;
                      {getTagName(tag)}
                    </Tag>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      )}
    </StyledThreadCreation>
  );
};

export default ThreadCreationComponent;
