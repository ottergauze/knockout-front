import React from 'react';
import PropTypes from 'prop-types';
import ThreadItem from '../../../components/ThreadItem';

const AlertsListThreadItem = ({
  id,
  createdAt,
  deleted,
  iconId,
  user,
  lastPost,
  locked,
  postCount,
  unreadPostCount,
  title,
  firstUnreadId,
  backgroundUrl,
  backgroundType,
  markUnreadAction,
  checkboxAction,
  checked,
}) => {
  return (
    <ThreadItem
      id={id}
      createdAt={createdAt}
      deleted={deleted}
      iconId={iconId}
      user={user}
      lastPost={lastPost}
      locked={locked}
      postCount={postCount}
      unreadPostCount={unreadPostCount}
      title={title}
      firstUnreadId={firstUnreadId}
      backgroundUrl={backgroundUrl}
      backgroundType={backgroundType}
      markUnreadAction={markUnreadAction}
      markUnreadText="Unsubscribe"
      showTopRating={false}
      includeCheckbox
      checkboxAction={checkboxAction}
      checked={checked}
    />
  );
};

AlertsListThreadItem.propTypes = {
  id: PropTypes.number.isRequired,
  createdAt: PropTypes.string.isRequired,
  deleted: PropTypes.bool,
  iconId: PropTypes.number.isRequired,
  user: PropTypes.shape({
    avatarUrl: PropTypes.string,
    username: PropTypes.string.isRequired,
    roleCode: PropTypes.string,
  }).isRequired,
  lastPost: PropTypes.shape({
    id: PropTypes.number.isRequired,
    createdAt: PropTypes.string.isRequired,
    user: PropTypes.shape({
      avatarUrl: PropTypes.string,
      username: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  locked: PropTypes.bool,
  postCount: PropTypes.number.isRequired,
  unreadPostCount: PropTypes.number,
  title: PropTypes.string.isRequired,
  firstUnreadId: PropTypes.number,
  backgroundUrl: PropTypes.string,
  backgroundType: PropTypes.string,
  markUnreadAction: PropTypes.func.isRequired,
  checkboxAction: PropTypes.func.isRequired,
  checked: PropTypes.bool.isRequired,
};

AlertsListThreadItem.defaultProps = {
  deleted: false,
  locked: false,
  unreadPostCount: 0,
  firstUnreadId: undefined,
  backgroundUrl: '',
  backgroundType: '',
};

export default AlertsListThreadItem;
