import React, { useEffect, useRef, useState } from 'react';
import { pushSmartNotification } from '../../../utils/notification';
import { checkUsername, updateUser } from '../../../services/user';
import Modal from '../../../components/Modals/Modal';
import { TextField } from '../../../components/FormControls';
import { useAppSelector } from '../../../state/hooks';
import InputError from '../../../components/InputError';

interface ChangeUsernameProps {
  modalOpen: boolean;
  setModalOpen: Function;
}

const ChangeUsername = ({ modalOpen, setModalOpen }: ChangeUsernameProps) => {
  const { username: accountUsername, id } = useAppSelector((state) => state.user);
  const [username, setUsername] = useState(accountUsername);
  const [errorMessage, setErrorMessage] = useState('');
  const checkTimeout = useRef<NodeJS.Timeout>();

  const checkInput = (input: string) => {
    clearTimeout(checkTimeout.current);
    setErrorMessage('');
    setUsername(input);
    if (input && input !== accountUsername) {
      checkTimeout.current = setTimeout(async () => {
        try {
          await checkUsername(input, id);
        } catch (error) {
          setErrorMessage(error?.response?.data?.message || 'This username is not valid.');
        }
      }, 600);
    }
  };

  useEffect(() => {
    if (modalOpen) setUsername(accountUsername);
  }, [modalOpen, accountUsername]);

  return (
    <Modal
      title="Change username"
      submitText="Change username"
      submitFn={async () => {
        const result = await updateUser({ username }, id);
        pushSmartNotification(result);
        window.location.reload();
      }}
      cancelFn={() => setModalOpen(false)}
      isOpen={modalOpen}
      disableSubmit={username.length < 3 || errorMessage || accountUsername === username}
    >
      <p>Your account&apos;s username can only be changed once per year.</p>
      <TextField
        placeholder="Enter your username"
        value={username}
        onChange={(e) => checkInput(e.target.value)}
      />
      <InputError error={errorMessage} />
    </Modal>
  );
};

export default ChangeUsername;
