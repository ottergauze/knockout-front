/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import OptionsStorageInput, { StyledOptionsStorageInput } from './OptionsStorageInput';
import ServerSideInput from './ServerSideInput';

import { getProfileRatingsDisplay, updateProfileRatingsDisplay } from '../../../services/user';
import { updateHeader, updateWidth } from '../../../state/style';
import { Panel, PanelTitle } from '../../../components/Panel';
import { setWidthToStorage } from '../../../services/theme';
import { ThemeTextColor } from '../../../utils/ThemeNew';
import { FieldLabelWithIcon, FieldLabelSmall } from '../../../components/FormControls';

export const autoSubscribeKey = 'autoSubscribe';
export const ratingsXrayKey = 'ratingsXray';
export const stickyHeaderKey = 'stickyHeader';
export const holidayThemeKey = 'holidayTheme';
export const punchyLabsKey = 'punchyLabs';
export const displayCountryInfoKey = 'displayCountryInfo';
export const nsfwFilterKey = 'nsfwFilter';
export const threadAdsKey = 'threadAds';
export const hideRatingsKey = 'hideRatings';
export const hideRiskyMedia = 'hideRiskyMedia';

const ThemeInputContainer = (props) => {
  const { label, value, options, desc } = props;
  return (
    <StyledOptionsStorageInput>
      <div className="options-info">
        <FieldLabelWithIcon label={label} icon="fa-arrows-alt-h" />
        <FieldLabelSmall>{desc}</FieldLabelSmall>
      </div>
      <div className="dropdown">
        <select onChange={(e) => props.update(e.target.value)} defaultValue={value}>
          {options.map((option) => {
            return (
              <option key={option.key} value={option.key}>
                {option.label}
              </option>
            );
          })}
        </select>
      </div>
    </StyledOptionsStorageInput>
  );
};

ThemeInputContainer.propTypes = {
  update: PropTypes.func,
  value: PropTypes.string,
  label: PropTypes.string,
  // eslint-disable-next-line react/forbid-prop-types
  options: PropTypes.arrayOf(PropTypes.any),
  desc: PropTypes.string,
};

ThemeInputContainer.defaultProps = {
  update: null,
  value: '',
  label: '',
  options: null,
  desc: '',
};

const StyledUserExperienceContainer = styled.div`
  .divider {
    margin: 20px 20px;
    color: ${ThemeTextColor};
    opacity: 0.3;
  }
`;

const UserExperienceContainer = () => {
  const width = useSelector((state) => state.style.width);
  const dispatch = useDispatch();

  const setWidth = (widthValue) => {
    dispatch(updateWidth(widthValue));
    setWidthToStorage(widthValue);
  };

  return (
    <StyledUserExperienceContainer>
      <Panel>
        <PanelTitle title="Because you're important to us <3">User Experience</PanelTitle>
        <div className="options-wrapper">
          <OptionsStorageInput
            desc="Subscribe to threads on reply / on creation"
            label="AutoSub™"
            icon="fa-bell"
            storageKey={autoSubscribeKey}
            defaultValue
          />

          <OptionsStorageInput
            desc="Hide media from new or inactive users"
            label="EyeSafe™"
            icon="fa-eye-slash"
            storageKey={hideRiskyMedia}
            defaultValue
          />

          <OptionsStorageInput
            desc="Show which country you're posting from"
            label="FlagPunchy™"
            icon="fa-flag"
            storageKey={displayCountryInfoKey}
            defaultValue={false}
          />

          <OptionsStorageInput
            desc="Hides NSFW threads"
            label="WorkSafe™"
            icon="fa-briefcase"
            storageKey={nsfwFilterKey}
            defaultValue
          />

          <hr className="divider" />

          <ServerSideInput
            desc="Hide ratings displayed on your profile"
            label="BoxHide™"
            icon="fa-box"
            setValue={updateProfileRatingsDisplay}
            getValue={getProfileRatingsDisplay}
          />

          <OptionsStorageInput
            desc="Hide all ratings on the site"
            label="BoxHide Deluxe™"
            icon="fa-boxes-stacked"
            storageKey={hideRatingsKey}
            defaultValue={false}
          />

          <OptionsStorageInput
            desc="See who left that *dumb* rating"
            label="Ratings X-ray™"
            icon="fa-x-ray"
            storageKey={ratingsXrayKey}
            defaultValue={false}
          />

          <hr className="divider" />

          <OptionsStorageInput
            desc="Enable holiday themes"
            label="HolidayTheme™"
            storageKey={holidayThemeKey}
            defaultValue
            icon="fa-snowflake"
            onChange={() => window.location.reload()}
          />

          <OptionsStorageInput
            desc="Experimental and buggy settings"
            label="PunchyLabs™"
            icon="fa-flask"
            storageKey={punchyLabsKey}
            defaultValue={false}
            onChange={() => window.location.reload()}
          />

          <OptionsStorageInput
            desc="Sticky header that's always with you as you scroll"
            label="StickyHeader™"
            icon="fa-sticky-note"
            storageKey={stickyHeaderKey}
            defaultValue
            onChange={(value) => dispatch(updateHeader(value))}
          />

          <OptionsStorageInput
            desc="Show short GIF 'thread advertisements' on the front page"
            label="ThreadAds™"
            icon="fa-ad"
            storageKey={threadAdsKey}
            defaultValue
          />

          <ThemeInputContainer
            label="Width"
            desc="Width of the middle section"
            update={setWidth}
            options={[
              { key: 'full', label: 'Full' },
              { key: 'verywide', label: 'Very Wide' },
              { key: 'wide', label: 'Wide' },
              { key: 'medium', label: 'Medium' },
              { key: 'narrow', label: 'Narrow' },
            ]}
            value={width}
          />
        </div>
      </Panel>
    </StyledUserExperienceContainer>
  );
};
export default UserExperienceContainer;
