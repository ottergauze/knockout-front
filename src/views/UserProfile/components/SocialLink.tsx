import React from 'react';
import styled from 'styled-components';

const StyledSocialElement = styled.a`
  opacity: 60%;
  display: flex;
  align-items: center;
  margin-bottom: 12px;
  transition: 0.4s;

  ${(props) =>
    props.href &&
    `&:hover {
    opacity: 40%;
  }`}

  .social-link-icon {
    margin-right: 7px;
    font-size: 20px;
    width: 25px;
    text-align: center;
  }
`;

interface SocialLinkProps {
  iconClass: string;
  link?: string;
  text: string;
}

const SocialLink = ({ iconClass, link = undefined, text }: SocialLinkProps) =>
  text && (
    <StyledSocialElement as={link ? 'a' : 'span'} href={link} target="_blank">
      <i className={`${iconClass} social-link-icon`} />
      <span style={{ overflow: 'hidden', whiteSpace: 'nowrap', textOverflow: 'ellipsis' }}>
        {text}
      </span>
    </StyledSocialElement>
  );

export default SocialLink;
