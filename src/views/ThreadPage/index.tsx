import React, { useEffect, useState } from 'react';
import { useHistory, useLocation, useRouteMatch } from 'react-router-dom';

import { Post, ThreadWithPosts } from 'knockout-schema';
import { Dispatch } from 'redux';
import { createAlertRequest, deleteAlertRequest } from '../../services/alerts';
import { createReadThreadRequest } from '../../services/readThreads';
import { getThreadWithPosts, getThread, updateThread } from '../../services/threads';
import { changeThreadStatus } from '../../services/moderation';

import { scrollToBottom, scrollIntoView } from '../../utils/pageScroll';
import { pushNotification, pushSmartNotification } from '../../utils/notification';
import { getSubforumList } from '../../services/subforums';
import { loadAutoSubscribeFromStorageBoolean } from '../../services/theme';
import { updateBackgroundRequest } from '../../state/background';
import { markNotificationsAsRead } from '../../services/notifications';
import { readNotification } from '../../state/notifications';
import {
  updateSubscriptionThread,
  removeSubscriptionThread,
  addSubscriptionThread,
} from '../../state/subscriptions';

import ThreadPageComponent from './ThreadPage';
import { POSTS_PER_PAGE } from '../../utils/postsPerPage';
import socketClient from '../../socketClient';
import { useAppDispatch, useAppSelector } from '../../state/hooks';

const defaultDate = new Date('01/01/1980');

const updateAlert = async (
  iconId,
  title,
  posts,
  threadId,
  previousLastSeen,
  previousLastPostNumber,
  isSubscribed = false,
  totalPosts = 1,
  currentPage = 1,
  locked = false,
  dispatch?: Dispatch
) => {
  if (posts.length > 0) {
    let result;
    let lastPostNumber = Number(posts[posts.length - 1].threadPostNumber);
    if (!lastPostNumber) {
      // if we cant find the last post number through the post object, calculate it manually
      lastPostNumber = (currentPage - 1) * POSTS_PER_PAGE + posts.length;
    }

    if (previousLastPostNumber !== undefined) {
      result = await createAlertRequest({
        threadId,
        lastPostNumber,
        previousLastPostNumber,
      });
    } else {
      result = await createAlertRequest({
        threadId,
        lastPostNumber,
        previousLastPostNumber: 0,
      });
    }

    if (result !== null && isSubscribed) {
      const remainingPosts = totalPosts - lastPostNumber;
      const threadInfo = {
        id: threadId,
        count: remainingPosts,
        title,
        page: currentPage + Number(remainingPosts > 0 || totalPosts % POSTS_PER_PAGE === 0),
        postNum: lastPostNumber,
        iconId,
        locked,
      };
      if (
        (previousLastPostNumber === 0 || previousLastSeen === defaultDate) &&
        remainingPosts > 0
      ) {
        dispatch!(addSubscriptionThread(threadInfo));
      } else if (remainingPosts === 0) {
        dispatch!(removeSubscriptionThread(threadId));
      } else {
        dispatch!(updateSubscriptionThread(threadInfo));
      }
    }
  }
};

const updateReadThread = async (posts: Post[], threadId: number, previousLastPostNumber = -1) => {
  if (posts.length > 0 && posts[posts.length - 1]?.threadPostNumber > previousLastPostNumber) {
    await createReadThreadRequest(
      threadId,
      posts[posts.length - 1].threadPostNumber,
      previousLastPostNumber
    );
  }
};

const ThreadPage = () => {
  const dispatch = useAppDispatch();

  const history = useHistory();
  const location = useLocation();
  const match = useRouteMatch<{ id: string; page: string }>();

  const userState = useAppSelector((state) => state.user);
  const notifications = useAppSelector((state) => state.notifications.notifications);

  const [thread, setThread] = useState<ThreadWithPosts>({
    posts: [],
    user: {
      username: '',
    },
    id: undefined,
  } as any);
  const [newPostQueue, setNewPostQueue] = useState<Post[]>([]);
  const [moveModal, setMoveModal] = useState({
    show: false,
    options: [] as {
      value: number;
      text: string;
    }[],
  });

  interface PostJumpParams {
    goToLatest: boolean;
    goToPost: boolean;
  }

  const [postJump, setPostJump] = useState<PostJumpParams>({ goToLatest: false, goToPost: false });

  useEffect(() => {
    setNewPostQueue([]);
    socketClient.emit('threadPosts:join', match.params.id);
    return () => {
      socketClient.emit('threadPosts:leave', match.params.id);
    };
  }, [match.params.id]);

  useEffect(() => {
    setNewPostQueue([]);
    socketClient.on('threadPost:new', (value: Post) => {
      if (value.page === Number(match.params.page) && value.user!.id !== userState.id) {
        setNewPostQueue((prevQueue) => [...prevQueue, value]);
      }
      setThread((prevThread) => ({ ...prevThread, postCount: prevThread.postCount + 1 }));
    });
    return () => {
      socketClient.off('threadPost:new');
    };
  }, [match.params.page]);

  useEffect(() => {
    // remove posts from the new post queue that are already in the thread
    const unfetchedPosts = newPostQueue.filter(
      (newPost) => !thread.posts.map((existingPost) => existingPost.id).includes(newPost.id)
    );
    setNewPostQueue(unfetchedPosts);
  }, [thread]);

  const addNewPosts = () => {
    setThread((prevThread) => {
      if (prevThread.subscribed) {
        updateAlert(
          prevThread.iconId,
          prevThread.title,
          [...prevThread.posts, ...newPostQueue],
          prevThread.id,
          undefined,
          prevThread.subscriptionLastPostNumber || undefined,
          prevThread.subscribed !== undefined,
          prevThread.postCount,
          prevThread.currentPage,
          prevThread.locked,
          dispatch
        );
      }

      return {
        ...prevThread,
        posts: [...prevThread.posts, ...newPostQueue],
      };
    });
    setNewPostQueue([]);
  };

  /**
   * @param object single object parameter, contains:
   * @param goToLatest boolean
   * @param goToPost boolean
   */
  const jumpToPost = ({ goToLatest, goToPost }) => {
    if (goToLatest) {
      scrollToBottom(1);
    } else if (goToPost) {
      // scroll to anchor in the url
      scrollIntoView(location.hash);
    }
  };

  const refreshPosts = async ({
    threadId = match.params.id,
    page = match.params.page,
    goToLatest = false,
    goToPost = false,
  } = {}) => {
    try {
      // this will be used to decide between default param or last page
      let targetPage = Number(page) || 1;

      // get the number of the last page
      if (goToLatest) {
        const pureThread = await getThread(threadId);
        const { totalPosts } = pureThread;
        const totalPages = Math.ceil(totalPosts / POSTS_PER_PAGE);
        history.push(`/thread/${threadId}/${totalPages}`);
        targetPage = totalPages;
      }

      // common thread updating logic
      const threadWithPosts = await getThreadWithPosts(threadId, targetPage);

      const newThread = {
        ...threadWithPosts,
        posts: [...threadWithPosts.posts].sort((a, b) => a.id - b.id),
      };

      // handle updating Alerts (subscriptions)
      if (
        (threadWithPosts.subscribed || goToLatest) &&
        (newThread.subscriptionLastPostNumber || newThread.subscriptionLastSeen)
      ) {
        await updateAlert(
          newThread.iconId,
          newThread.title,
          newThread.posts,
          newThread.id,
          newThread.subscriptionLastSeen || undefined,
          newThread.subscriptionLastPostNumber || undefined,
          threadWithPosts.subscribed !== undefined,
          newThread.totalPosts,
          newThread.currentPage,
          newThread.locked,
          dispatch
        );
      } else if (goToLatest && loadAutoSubscribeFromStorageBoolean()) {
        socketClient.emit('subscribedThreadPosts:join', thread.id);
        await updateAlert(
          newThread.iconId,
          newThread.title,
          newThread.posts,
          newThread.id,
          undefined,
          0
        );
        await refreshPosts({});
      }

      // handle updating ReadThreads if user is logged in
      if (userState?.id) {
        updateReadThread(newThread.posts, newThread.id, newThread.readThreadLastPostNumber);
      }

      // if we have a thread background, let's set it!
      dispatch(
        updateBackgroundRequest(newThread.threadBackgroundUrl, newThread.threadBackgroundType)
      );

      // sets state with the new thread data
      setThread(newThread);
      setPostJump({ goToLatest, goToPost });
    } catch (error) {
      console.error(error);
    }
  };

  const createAlert = async (posts, threadId, showNotification = false) => {
    try {
      // if we're creating a new Alert, the previous post number is 0
      await updateAlert(
        thread.iconId,
        thread.title,
        posts,
        threadId,
        undefined,
        0,
        true,
        thread.postCount,
        thread.currentPage,
        thread.locked,
        dispatch
      );

      socketClient.emit('subscribedThreadPosts:join', threadId);
      refreshPosts({});

      if (showNotification) {
        pushNotification({ message: 'Success! You are now subscribed.', type: 'success' });
      }
    } catch (error) {
      pushNotification({
        message: 'Oops, we could not subscribe you to this thread. Sorry.',
        type: 'warning',
      });
    }
  };

  const deleteAlert = async (threadId) => {
    try {
      await deleteAlertRequest({ threadId });
      socketClient.emit('subscribedThreadPosts:leave', threadId);
      pushNotification({ message: 'No longer subscribed to thread.', type: 'success' });
      dispatch(removeSubscriptionThread(threadId));
      refreshPosts({});
    } catch (e) {
      console.error(e);
    }
  };

  const updateThreadStatus = async (status) => {
    try {
      const actions: {
        unlock?: boolean;
        lock?: boolean;
        restore?: boolean;
        delete?: boolean;
        unpin?: boolean;
        pinned?: boolean;
      } = {};

      if (status.locked) {
        actions.unlock = true;
      }
      if (status.locked === false) {
        actions.lock = true;
      }
      if (status.deleted) {
        actions.restore = true;
      }
      if (status.deleted === false) {
        actions.delete = true;
      }
      if (status.pinned) {
        actions.unpin = true;
      }
      if (status.pinned === false) {
        actions.pinned = true;
      }

      await changeThreadStatus({ threadId: thread.id, statuses: actions });
      await refreshPosts();
    } catch (err) {
      pushNotification({ message: 'Error!', type: 'error' });
    }
  };

  const showMoveModal = async () => {
    const subforums = await getSubforumList();
    const subforumOptions = subforums.map((subforum) => ({
      value: subforum.id,
      text: subforum.name,
    }));

    setMoveModal({ show: true, options: subforumOptions });
  };

  const submitThreadMove = async (id) => {
    if (!thread.id) {
      pushSmartNotification({ error: 'Invalid thread.' });
      throw new Error('Invalid thread.');
    }
    await updateThread(thread.id, { subforum_id: Number(id) });

    setMoveModal({ show: false, options: [] });
    pushNotification({ message: 'Thread moved.', type: 'success' });
  };

  const markUnreadNotificationsInPosts = async (
    posts: Post[],
    notificationType: string
  ): Promise<void> => {
    const unreadNotificationPostIds = posts
      .map((post) => {
        if (
          `${notificationType}:${post.id}` in notifications &&
          !notifications[`${notificationType}:${post.id}`].read
        ) {
          return post.id;
        }
        return null;
      })
      .filter((postId) => Boolean(postId));

    if (unreadNotificationPostIds.length === 0) return;

    await markNotificationsAsRead(
      unreadNotificationPostIds.map((id) => notifications[`${notificationType}:${id}`].id)
    );
    unreadNotificationPostIds.forEach((postId) => {
      dispatch(readNotification(`${notificationType}:${postId}`));
    });
  };

  const markMentionsAsRead = async () => {
    if (Object.keys(notifications).length === 0) return;

    const { posts } = thread;

    try {
      await markUnreadNotificationsInPosts(posts, 'POST_REPLY');
      await markUnreadNotificationsInPosts(posts, 'POST_MENTION');
    } catch (e) {
      console.error(e);
    }
  };

  const togglePinned = () => updateThreadStatus({ pinned: thread.pinned });

  const toggleLocked = () => updateThreadStatus({ locked: thread.locked });

  const toggleDeleted = () => updateThreadStatus({ deleted: thread.deleted });

  const createAlertFn = () => createAlert(thread.posts, thread.id, true);

  const hideModal = () => setMoveModal({ show: false, options: [] });

  useEffect(() => {
    refreshPosts({ threadId: match.params.id, page: match.params.page, goToPost: true });
    return () => {
      dispatch(updateBackgroundRequest(null));
    };
  }, [match.params.id, match.params.page]);

  useEffect(() => {
    setTimeout(() => {
      jumpToPost(postJump);
      markMentionsAsRead();
    }, 500);
  }, [postJump]);

  const { hash } = location;

  const postId = Number(hash?.replace('#post-', '')) || undefined;

  const { params } = match;
  const currentPage = params.page ? Number(params.page) : 1;

  return (
    <ThreadPageComponent
      thread={thread}
      currentPage={currentPage}
      showingMoveModal={moveModal.show}
      moveModalOptions={moveModal.options}
      togglePinned={togglePinned}
      toggleLocked={toggleLocked}
      toggleDeleted={toggleDeleted}
      showMoveModal={showMoveModal}
      deleteAlert={() => deleteAlert(thread.id)}
      createAlert={createAlertFn}
      refreshPosts={refreshPosts}
      currentUserId={userState.id}
      submitThreadMove={submitThreadMove}
      hideModal={hideModal}
      params={params}
      linkedPostId={postId}
      newPostQueue={newPostQueue}
      addNewPosts={addNewPosts}
    />
  );
};

export default ThreadPage;
