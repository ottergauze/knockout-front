import React, { useEffect, useRef, useState } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import { transparentize } from 'polished';
import { ShortcodeTree } from 'shortcode-tree';
import {
  BanEvent,
  EventType,
  Post,
  PostEvent,
  Thread,
  ThreadEvent,
  User,
  UserEvent,
} from 'knockout-schema';
import UserRoleWrapper from '../../../components/UserRoleWrapper';
import humanizeDuration from '../../../utils/humanizeDuration';
import {
  ThemeFontSizeSmall,
  ThemeHighlightStronger,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import ratingList from '../../../utils/ratingList.json';
import { useAppSelector } from '../../../state/hooks';

dayjs.extend(relativeTime);

interface StyledEventProps {
  reply?: boolean;
}

const StyledEvent = styled.div<StyledEventProps>`
  display: flex;
  margin-bottom: 8px;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  line-height: 1.5em;
  align-items: center;
  overflow-y: hidden;
  ${(props) => props.reply && `background: ${transparentize(0.8, ThemeHighlightStronger(props))}`};

  .event-link {
    color: #3facff;
  }

  .event-highlight {
    font-weight: 600;
    opacity: 0.7;
  }

  .event-text {
    flex-grow: 1;
  }

  .event-icon {
    font-size: 24px;
    width: 24px;
    margin-right: calc(${ThemeHorizontalPadding} * 2);
    text-align: center;
    display: flex;
    justify-content: center;
  }

  .rating-icon {
    width: 24px;
  }

  .event-user {
    font-weight: 600;
  }

  .event-time {
    margin-left: 8px;
    opacity: 0.5;
    font-size: ${ThemeFontSizeSmall};
    white-space: nowrap;
  }
`;

interface EventProps {
  event: ThreadEvent | UserEvent | BanEvent | PostEvent;
}

const Event = ({ event }: EventProps) => {
  const currentUserId = useAppSelector((state) => state.user.id);
  const [banDuration, setBanDuration] = useState('');
  const iconRef = useRef<HTMLSpanElement>(null);

  const getThread = (thread: Thread = event.data as Thread, text = thread.title, post?: Post) => (
    <Link
      className="event-link"
      to={`/thread/${thread.id}${post ? `/${post.page}#post-${post.id}` : ''}`}
    >
      {text}
    </Link>
  );
  const getUser = (user: User = event.data as User, username = user.username) => (
    <Link to={`/user/${user.id}`} className="event-user">
      <UserRoleWrapper user={user}>{username}</UserRoleWrapper>
    </Link>
  );

  const mentionsUser = (content) => {
    const tree = ShortcodeTree.parse(content);
    return tree.children.some(
      (node) =>
        node.shortcode?.name === 'quote' &&
        Number(node.shortcode?.properties?.mentionsUser) === currentUserId
    );
  };

  useEffect(() => {
    if ((window as any).twemoji)
      (window as any).twemoji.parse(iconRef.current, {
        base: 'https://cdn.jsdelivr.net/gh/twitter/twemoji@14.0.2/assets/',
      });
    if (event.type === 'user-banned') {
      setBanDuration(humanizeDuration(event.data.createdAt, event.data.expiresAt));
    }
  }, []);

  const creator = getUser(event.creator);
  let text:
    | (string | any[] | React.ReactElement<any, string | React.JSXElementConstructor<any>>)[]
    | string;
  let emoji: string;
  let reply: boolean | undefined;
  const ratingObject = ratingList[event.content.rating];
  switch (event.type) {
    case 'thread-created':
      emoji = '📰';
      text = [creator, ' created ', getThread()];
      break;
    case 'thread-background-updated':
      emoji = '🖼️';
      text = [creator, ' updated the background of ', getThread()];
      reply = event.data.user === currentUserId;
      break;
    case 'thread-deleted':
      emoji = '🗑️';
      text = [creator, ' deleted ', getThread(event.data, 'a thread')];
      reply = event.data.user === currentUserId;
      break;
    case 'thread-moved':
      emoji = '🚚';
      text = [
        creator,
        ' moved ',
        getThread(),
        ' to ',
        <Link className="event-link" key={0} to={`subforum/${event.data.subforum.id}`}>
          {event.data.subforum.name}
        </Link>,
      ];
      reply = event.data.user === currentUserId;
      break;
    case 'thread-pinned':
      emoji = '📌';
      text = [creator, ' pinned ', getThread()];
      reply = event.data.user === currentUserId;
      break;
    case 'thread-unpinned':
      emoji = '♻️';
      text = [creator, ' unpinned ', getThread()];
      reply = event.data.user === currentUserId;
      break;
    case 'thread-locked':
      emoji = '🔒';
      text = [creator, ' locked ', getThread()];
      reply = event.data.user === currentUserId;
      break;
    case 'thread-unlocked':
      emoji = '🔓';
      text = [creator, ' unlocked ', getThread()];
      reply = event.data.user === currentUserId;
      break;
    case 'thread-renamed':
      emoji = '✒️';
      text = [
        creator,
        ' renamed ',
        <span key={0} className="event-highlight">
          {event.content.oldTitle}
        </span>,
        ' to ',
        getThread(event.data, event.content.newTitle),
      ];
      reply = event.data.user === currentUserId;
      break;
    case 'thread-restored':
      emoji = '🔄';
      text = [creator, ' restored ', getThread()];
      reply = event.data.user === currentUserId;
      break;
    case 'thread-post-limit-reached':
      emoji = '🛑';
      text = [getThread(), ' reached its post limit and was automatically locked'];
      reply = event.data.user === currentUserId;
      break;
    case 'user-avatar-removed':
      emoji = '🔞';
      text = [creator, ' removed ', getUser(), "'s avatar because of their poor taste."];
      reply = event.data.id === currentUserId;
      break;
    case 'user-background-removed':
      emoji = '🔞';
      text = [creator, ' removed ', getUser(), "'s background because of their poor taste."];
      reply = event.data.id === currentUserId;
      break;
    case 'user-unbanned':
      emoji = '👏';
      text = [creator, ' reversed one of ', getUser(), "'s mutes."];
      reply = event.data.id === currentUserId;
      break;
    case 'user-profile-removed':
      emoji = '🔞';
      text = [
        creator,
        ' removed ',
        getUser(),
        "'s profile customizations because of their poor taste.",
      ];
      reply = event.data.id === currentUserId;
      break;
    case 'user-banned':
      emoji = '⛔';
      text = [
        creator,
        ' muted ',
        getUser(event.data.user),
        event.data.post && [
          ' in ',
          getThread(event.data.thread, event.data.thread.title, event.data.post),
        ],
        ' with reason "',
        <span key={0} className="event-highlight">
          {event.data.banReason}
        </span>,
        '" for ',
        banDuration,
      ];
      reply = event.data.user.id === currentUserId;
      break;

    case EventType.BAN_REASON_EDITED:
      emoji = '🪛';
      text = [
        creator,
        ' edited the reason for ',
        getUser(event.data.user, 'a user'),
        '\'s ban from "',
        <span key={0} className="event-highlight">
          {event.content.oldReason}
        </span>,
        '" to "',
        event.content.newReason,
        '"',
      ];
      break;
    case 'user-wiped':
      emoji = '💥';
      text = [
        creator,
        ' permanently banned and wiped ',
        getUser(event.data.user, 'a user'),
        '\'s account, with reason "',
        <span key={0} className="event-highlight">
          {event.data.banReason}
        </span>,
        '"',
      ];
      break;
    case 'post-created':
      emoji = '💬';
      text = [
        creator,
        ' created a new post in ',
        getThread(event.data.thread, event.data.thread!.title, event.data),
      ];
      reply = mentionsUser(event.data.content);
      break;
    case 'rating-created':
      emoji = ratingObject && (
        <img className="rating-icon" src={ratingObject.url} alt={ratingObject.name} />
      );
      text = [
        creator,
        ' rated a post in ',
        getThread(event.data.thread, event.data.thread!.title, event.data),
      ];
      reply = event.data.userId === currentUserId;
      break;
    case 'profile-comment-created':
      emoji = '📝';
      text = [creator, ' commented on ', getUser(), "'s profile"];
      reply = event.data.id === currentUserId;
      break;
    case 'gold-earned':
      emoji = '🏆';
      text = [creator, ' is now a Gold Member'];
      break;
    case 'gold-lost':
      emoji = '💸';
      text = [creator, ' is no longer a Gold Member'];
      break;
    default:
      emoji = '⚠️';
      text = 'Problem retrieving event data.';
      break;
  }

  return (
    <StyledEvent reply={reply}>
      <span ref={iconRef} className="event-icon">
        {emoji}
      </span>
      <span className="event-text">{text}</span>
      <span className="event-time">{dayjs(event.createdAt).fromNow()}</span>
    </StyledEvent>
  );
};

export default Event;
