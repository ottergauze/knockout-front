import React from 'react';
import PropTypes from 'prop-types';
import ThreadItemPlaceholder from '../../../components/ThreadItemPlaceholder';
import BlankSlate from '../../../components/BlankSlate';
import ThreadSearchThreadItem from './ThreadSearchThreadItem';

const ThreadSearchResults = ({ totalThreads, threads, loading }) => {
  let resultsContent;

  if (totalThreads === 0 && !loading) {
    resultsContent = <BlankSlate resourceNamePlural="threads" />;
  } else if (loading) {
    resultsContent = Array(10)
      .fill(1)
      // eslint-disable-next-line react/no-array-index-key
      .map((_, index) => <ThreadItemPlaceholder key={`p${index}`} />);
  } else {
    resultsContent = threads?.map((thread) => {
      const user = {
        avatarUrl: thread.user.avatarUrl,
        username: thread.user.username,
        id: thread.user.id,
      };
      return (
        <ThreadSearchThreadItem
          key={thread.id}
          id={thread.id}
          createdAt={thread.createdAt}
          deleted={thread.deleted}
          iconId={thread.iconId}
          lastPost={thread.lastPost}
          locked={!!thread.locked}
          postCount={thread.postCount}
          title={thread.title}
          tags={thread.tags}
          user={user}
          backgroundUrl={thread.backgroundUrl}
          backgroundType={thread.backgroundType}
          subforumName={thread.subforum?.name}
        />
      );
    });
  }

  return <>{resultsContent}</>;
};

ThreadSearchResults.propTypes = {
  totalThreads: PropTypes.number,
  threads: PropTypes.arrayOf(PropTypes.object),
  loading: PropTypes.bool,
};

ThreadSearchResults.defaultProps = {
  totalThreads: 0,
  threads: [],
  loading: false,
};

export default React.memo(ThreadSearchResults);
