import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import React from 'react';
import styled from 'styled-components';
import { transparentize } from 'polished';
import { CalendarEvent } from 'knockout-schema';
import Modal from '../../../components/Modals/Modal';
import {
  ThemeFontSizeLarge,
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';

dayjs.extend(relativeTime);

const MAX_EVENT_TITLE_LENGTH = 50;

const StyledModalContent = styled.div`
  margin-top: 0;
  text-align: left;

  .event {
    display: block;
    text-align: left;
    font-size: ${ThemeFontSizeLarge};
    margin-left: 0;
    padding-left: 0;
    margin-bottom: ${ThemeVerticalPadding};
    background: none;
    border: none;
    cursor: pointer;
    color: ${ThemeTextColor};

    .start-time {
      color: ${(props) => transparentize(0.3, ThemeTextColor(props))};
      margin-right: ${ThemeHorizontalPadding};
    }

    &:hover {
      text-decoration: underline;
    }
  }
`;

interface CalendarDayModalProps {
  date: Date;
  events: CalendarEvent[];
  isOpen: boolean;
  close: () => void;
  onEventClick: (event: CalendarEvent) => void;
}

const CalendarDayModal = ({ date, events, isOpen, close, onEventClick }: CalendarDayModalProps) => {
  const formattedDate = date.toLocaleString('en-US', {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  });
  const title = `Events for ${formattedDate}`;

  return (
    <Modal
      iconUrl="/static/icons/planner.png"
      title={title}
      isOpen={isOpen}
      cancelFn={close}
      hideButtons
    >
      <StyledModalContent>
        {events.map((event) => {
          let truncatedTitle = event.title.slice(0, MAX_EVENT_TITLE_LENGTH);
          if (event.title.length > MAX_EVENT_TITLE_LENGTH) {
            truncatedTitle += '...';
          }
          const startDate = new Date(event.startsAt);
          const startTime = startDate.toLocaleString('en-US', {
            hour: 'numeric',
            minute: 'numeric',
            hour12: true,
          });
          return (
            <button
              type="button"
              key={event.id}
              onClick={() => onEventClick(event)}
              className="event"
            >
              <span className="start-time">{startTime}</span>
              <span className="event-title">{truncatedTitle}</span>
            </button>
          );
        })}
      </StyledModalContent>
    </Modal>
  );
};

export default CalendarDayModal;
