import { darken, lighten } from 'polished';
import { StyledProps } from 'styled-components';
import { ThemeBackgroundLighter } from '../../utils/ThemeNew';

export default (styleProps: StyledProps<unknown>) => {
  if (styleProps.theme.mode === 'light') return darken(0.05, ThemeBackgroundLighter(styleProps));
  return lighten(0.05, ThemeBackgroundLighter(styleProps));
};
