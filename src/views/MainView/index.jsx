/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { connect, useSelector } from 'react-redux';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Slide, ToastContainer } from 'react-toastify';
import Snowfall from 'react-snowfall';
import Footer from '../../components/Footer';
import Header from '../../components/Header';
import routes from '../../routes';
import ScrollOnRouteChange from './components/ScrollOnRouteChange';
import { GlobalStyle, Background, FlexWrapper } from './style';
import PassiveAggressiveness from './components/PassiveAggressiveness';
import { loadPunchyLabsFromStorageBoolean } from '../../services/theme';
import { loadValue } from '../../utils/postOptionsStorage';
import { holidayThemeKey } from '../UserSettingsPage/components/UserExperienceContainer';
import { isHolidays } from '../../utils/eventDates';

export const ConnectedGlobalStyle = connect(({ background }) => ({
  backgroundUrl: background.url,
}))(GlobalStyle);

export const ConnectedBackground = connect(({ background }) => ({
  url: background.url,
  bgType: background.type,
}))(Background);

const punchyLabsEnabled = loadPunchyLabsFromStorageBoolean();

const getPunchyLabsNoticeNotRead = () => {
  return punchyLabsEnabled && localStorage.getItem('punchyLabsNoticeRead') !== 'true';
};

const MainView = () => {
  const hasMotd = useSelector((state) => state.style.motd);
  const enableSnow = isHolidays() && loadValue(holidayThemeKey, true);

  return (
    <FlexWrapper hasMotd={hasMotd}>
      <ConnectedGlobalStyle />

      <Header />

      {enableSnow && (
        <Snowfall
          snowflakeCount={75}
          speed={[0.5, 1]}
          wind={[0.5, 1]}
          radius={[0.5, 3]}
          rotationSpeed={[0, 0.5]}
          changeFrequency={5000}
          style={{
            position: 'fixed',
            width: '100vw',
            height: '100vh',
            opacity: '0.6',
            zIndex: '999',
          }}
        />
      )}

      <ScrollOnRouteChange>
        <Switch>
          {routes.map((route) => (
            <Route key={route.name} {...route} />
          ))}
          <Redirect from="*" to="/" />
        </Switch>
      </ScrollOnRouteChange>

      <Footer />

      <ToastContainer transition={Slide} />

      <ConnectedBackground />

      {getPunchyLabsNoticeNotRead() && <PassiveAggressiveness />}
    </FlexWrapper>
  );
};
export default MainView;
