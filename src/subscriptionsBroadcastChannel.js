const channel = typeof BroadcastChannel === 'function' ? new BroadcastChannel('subscriptions') : {};
export default channel;
