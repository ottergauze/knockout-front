import mastodonLinkFromHandle from '../../src/utils/mastodonLinkFromHandle';

describe('mastodonLinkFromHandle', () => {
  it("should return a link to the user's Mastodon profile", () => {
    const handle = 'gargron@petal.me';
    expect(mastodonLinkFromHandle(handle)).toBe('https://petal.me/@gargron');
  });

  it('should return null if the handle is not a Mastodon handle', () => {
    const handle = 'gargron';
    expect(mastodonLinkFromHandle(handle)).toBe(null);
  });
});
