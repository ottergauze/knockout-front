/* eslint-disable no-underscore-dangle */
import React from 'react';
import { BASIC_USER, GOLD_USER, MODERATOR } from '../../../../src/utils/roleCodes';

import EditableBackground from '../../../../src/views/ThreadPage/components/EditableBackground';
import { customRender } from '../../../custom_renderer';
import store from '../../../../src/state/configureStore';

describe('EditableBackground component', () => {
  describe('as a non-logged in user', () => {
    it('does not display the button', () => {
      const { queryByText } = customRender(<EditableBackground />);
      expect(queryByText('Change background')).toBeNull();
    });
  });

  describe('as a logged in user', () => {
    const loggedInState = {
      user: {
        id: 1,
        username: 'test',
        loggedIn: true,
        role: { code: BASIC_USER },
        avatarUrl: 'avatar.png',
      },
    };

    beforeEach(() => {
      localStorage.__STORE__.currentUser = JSON.stringify(loggedInState.user);
      jest.spyOn(store, 'getState').mockImplementation(() => loggedInState);
    });

    describe('who is not the thread creator', () => {
      it('does not display the button', () => {
        const { queryByText } = customRender(<EditableBackground />, {
          initialState: loggedInState,
        });
        expect(queryByText('Change background')).toBeNull();
      });
    });

    describe('who is the thread creator', () => {
      it('does not allow the user to edit the background', () => {
        const { queryByTitle } = customRender(<EditableBackground byCurrentUser />, {
          initialState: loggedInState,
        });
        expect(queryByTitle('Change background (Must be a Gold member)')).not.toBeNull();
      });

      describe('and a gold member', () => {
        beforeEach(() => {
          loggedInState.user.role.code = GOLD_USER;
        });

        it('allows the user to edit the background', () => {
          const { queryByTitle } = customRender(<EditableBackground byCurrentUser />, {
            initialState: loggedInState,
          });
          expect(queryByTitle('Change background')).not.toBeNull();
        });
      });
    });

    describe('who is an admin', () => {
      beforeEach(() => {
        loggedInState.user.role.code = MODERATOR;
      });

      it('allows the user to edit the background', () => {
        const { queryByTitle } = customRender(<EditableBackground />, {
          initialState: loggedInState,
        });
        expect(queryByTitle('Change background')).not.toBeNull();
      });
    });
  });
});
